FROM jenkins/agent:4.10-5

USER root

RUN apt-get update && \
    apt-get install -y curl python wget jq && \
    curl -sSL https://get.docker.com/ | sh && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

RUN wget https://bootstrap.pypa.io/pip/2.7/get-pip.py && \
    python get-pip.py && \
    pip install yq

USER jenkins

# dockerhub.greensight.ru/services/dind-jenkins-agent:v2
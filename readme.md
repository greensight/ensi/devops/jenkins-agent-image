# Jenkins Agent

Образ агента для использования с [Docker plugin](https://plugins.jenkins.io/docker-plugin/).

Дополнительно установлены:
* docker-cli
* puthon 2.7
* curl

## Использование

В разделе `Настроить Jenkins > Управление средами сборки` переходим по ссылке `Configure clouds`.
Добавляем новый cloud, указываем адрес докер-демона.

Далее добавляем `Docker Agent template` с такими настройками

| name                     | value                                               |
|--------------------------|-----------------------------------------------------|
| Docker Image             | dockerhub.greensight.ru/services/jenkins-agent:4.10 |
| Remote File System Root  | /home/jenkins/agent                                 |
| Connect method           | Attach docker container                             |
| User                     | root                                                |

Далее открываем `Container settings` и прописываем

| name   | value                                                                  |
|--------|------------------------------------------------------------------------|
| Mounts | type=bind,source=/var/run/docker.sock,destination=/var/run/docker.sock |

Такие настройки позволяют из агента обращаться к докер-демону хоста, на котором запущен агент.  
При запуске контейнеров в пайплайне, они не будут запускаться внутри контейнера агента,
они будут созданы "рядом" с ним. Это стоит учитывать при монтировании папок.  
Лучше использовать [Docker pipeline plugin](https://plugins.jenkins.io/docker-workflow/),
который не только даёт удобный dsl для работы с докером, но ещё и автоматически монтирует текущую директорию
из контейнера агента в запускаемый рядом контейнер.